#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    srand( time(nullptr));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_linePushButton_clicked()
{
    double a, b;
    a = ui->lineASpin->value();
    b = ui->lineBSpin->value();
    if (fabs(a) < 0.000000001)
        ui->textEdit->insertPlainText("Неверно заданное линейное уравнение!\n");
    else {
        double s = linearEquation(a, b);
        ui->textEdit->insertPlainText("Корень линейного уравнения = " + QString::number(s) + "\n");
    }
}

void MainWindow::on_squarePushButton_clicked()
{
    double a, b, c;
    a = ui->squareASpin->value();
    b = ui->squareBSpin->value();
    c = ui->squareCSpin->value();
    if (fabs(a) < 0.000000001)
        ui->textEdit->insertPlainText("Неверно заданное квадратное уравнение!\n");
    else {
        if (b * b - 4 * a * c < 0)
            ui->textEdit->insertPlainText("Отсутствуют действительные корни квадратного уравнения.\n");
        else {
            QVector<double> s = squareEquation(a, b, c);
            if(fabs(s[0] - s[1]) < 0.000000001)
                ui->textEdit->insertPlainText("Корень квадратного уравнения = " + QString::number(s[0]) + "\n");
            else
                ui->textEdit->insertPlainText("Корни квадратного уравнения: " + QString::number(s[0]) + ", "
                        + QString::number(s[1]) + "\n");
        }
    }
}

void MainWindow::on_cubePushButton_clicked()
{
    double a, b, c, d;
    a = ui->cubeASpin->value();
    b = ui->cubeBSpin->value();
    c = ui->cubeCSpin->value();
    d = ui->cubeDSpin->value();
    if (fabs(a) < 0.000000001)
        ui->textEdit->insertPlainText("Неверно заданное кубическое уравнение!\n");
    else {
        QVector<double> s = cubeEquation(a, b, c, d);
        if(s.size() == 1) {
            ui->textEdit->insertPlainText("Корень кубического уравнения = " + QString::number(s[0]) + "\n");
        }
        else {
            ui->textEdit->insertPlainText("Корни кубического уравнения: " + QString::number(s[0]));
            for (int i = 1; i < s.size(); ++i)
                ui->textEdit->insertPlainText(", " + QString::number(s[i]));
            ui->textEdit->insertPlainText("\n");
        }
    }
}

void MainWindow::on_arithElemPushButton_clicked()
{
    double a, d;
    int n = ui->arithNSpin->value();
    if (ui->radioButton->isChecked()) {
        a = rand() % 200 - 100;
        d = rand() % 200 - 100;
    }
    else {
        a = ui->arithASpin->value();
        d = ui->arithDSpin->value();
    }
    double s = elemArithProgress(n, a, d);
    ui->textEdit->insertPlainText("Арифм. прогрессия(a1 = " + QString::number(a) + ", d = " + QString::number(d) + "): "
                                  + QString::number(n) + "-й элемент = " + QString::number(s) + "\n");
}

void MainWindow::on_arithSumPushButton_clicked()
{
    double a, d;
    int n = ui->arithNSpin->value();
    if (ui->radioButton->isChecked()) {
        a = rand() % 200 - 100;
        d = rand() % 200 - 100;
    }
    else {
        a = ui->arithASpin->value();
        d = ui->arithDSpin->value();
    }
    double s = sumArithProgress(n, a, d);
    ui->textEdit->insertPlainText("Арифм. прогрессия(a1 = " + QString::number(a) + ", d = " + QString::number(d) + "): "
                                  + "сумма по " + QString::number(n) + "-й элемент = " + QString::number(s) + "\n");
}

void MainWindow::on_geomSumPushButton_clicked()
{
    double b, q;
    int n = ui->geomNSpin->value();
    if (ui->radioButton_3->isChecked()) {
        b = rand() % 200 - 100;
        q = rand() % 200 - 100;
    }
    else {
        b = ui->geomBSpin->value();
        q = ui->geomQSpin->value();
    }
    double s = sumGeomProgress(n, b, q);
    ui->textEdit->insertPlainText("Геом. прогрессия(b1 = " + QString::number(b) + ", q = "
                                  + QString::number(q) + "): " + "сумма по " + QString::number(n)
                                  + "-й элемент = " + QString::number(s) + "\n");
}

void MainWindow::on_statSquarePushButton_clicked()
{
    double a, b, c;
    a = ui->squareASpin->value();
    b = ui->squareBSpin->value();
    c = ui->squareCSpin->value();
    ui->textEdit->insertPlainText("f(x) = " + QString::number(a) + "x^2 + " + QString::number(b) + "x + "
                                  + QString::number(c) + "; ");
    if (fabs(a) < 0.000000001) {
        ui->textEdit->insertPlainText("Стационарные точки отсутствуют\n");
    }
    else {
        double s = statSquare(a, b);
        ui->textEdit->insertPlainText("Стационарная точка = " + QString::number(s) + "\n");
    }
}

void MainWindow::on_statCubeButton_clicked()
{
    double a, b, c, d;
    a = ui->cubeASpin->value();
    b = ui->cubeBSpin->value();
    c = ui->cubeCSpin->value();
    d = ui->cubeDSpin->value();
    ui->textEdit->insertPlainText("f(x) = " + QString::number(a) + "x^3 + " + QString::number(b) + "x^2 + "
                                  + QString::number(c) + "x + " + QString::number(d) + "; ");
    if (b*2 * b*2 - 4 * a*3 * c < 0) {
        ui->textEdit->insertPlainText("Стационарные точки отсутствуют\n");
    }
    else if (fabs(a) < 0.000000001) {
        if (fabs(b) < 0.000000001) {
            ui->textEdit->insertPlainText("Стационарные точки отсутствуют\n");
        }
        else {
            double s = statSquare(a, b);
            ui->textEdit->insertPlainText("Стационарная точка = " + QString::number(s) + "\n");
        }
    }
    else {
        QVector<double> s = statCube(a, b, c);
        if(fabs(s[0] - s[1]) < 0.000000001) {
            ui->textEdit->insertPlainText("Стационарная точка = " + QString::number(s[0]) + "\n");
        }
        else {
            ui->textEdit->insertPlainText("Стационарные точки: " + QString::number(s[0]) + ", "
                    + QString::number(s[1]) + "\n");
        }
    }
}
