#include <QtTest>
#include "../functions.cpp"
#include "tst_check.h"
// Это тесты
check::check()
{
}

bool compareVec(const QVector<double> &vec1, const QVector<double> &vec2) {
    if (vec1 == vec2)
        return true;
    return false;
}

void check::testLinearEquation()
{
    double a = 1, b = 2;
    QCOMPARE(-2, linearEquation(a, b));

    a = -7.5;
    b = 22.5;
    QCOMPARE(3, linearEquation(a, b));
}

void check::testSquareEquation()
{
    double a = 1, b = 2, c = -3;
    QCOMPARE(true, compareVec(squareEquation(a, b, c), {1, -3}) || compareVec(squareEquation(a, b, c), {-3, 1}));

    a = 1;
    b = 0;
    c = -4;
    QCOMPARE(true, compareVec(squareEquation(a, b, c), {2, -2}) || compareVec(squareEquation(a, b, c), {-2, 2}));
}

void check::testCubeEquation()
{
    double a = 1, b = 1, c = 1, d = 1;
    QVector<double> s = cubeEquation(a, b, c, d);
    QCOMPARE(-1., s[0]);

    a = 1;
    b = 1;
    c = -1;
    d = -1;
    s = cubeEquation(a, b, c, d);
    QCOMPARE(1., s[0]);
}

void check::testElemArithProgress()
{
    int n = 3;
    double a = 2, b = 1;
    QCOMPARE(4, elemArithProgress(n, a, b));

    n = 2;
    a = 8;
    b = 12;
    QCOMPARE(20, elemArithProgress(n, a, b));
}
void check::testSumArithProgress()
{
    int n = 3;
    double a = 2, b = 1;
    QCOMPARE(9, sumArithProgress(n, a, b));

    n = 2;
    a = 8;
    b = 12;
    QCOMPARE(28, sumArithProgress(n, a, b));
}

void check::testSumGeomProgress()
{
    int n = 3;
    double b = 2, q = 2;
    QCOMPARE(14, sumGeomProgress(n, b, q));

    n = 2;
    b = 8;
    q = 3;
    QCOMPARE(32, sumGeomProgress(n, b, q));
}

void check::testStatCube()
{
    double a = 1, b = 0, c = -3, d = 22;
    QCOMPARE(true, compareVec(statCube(a, b, c), {1, -1}) || compareVec(statCube(a, b, c), {-1, 1}));

    a = 2;
    b = 3;
    c = -12;
    d = 113;
    QCOMPARE(true, compareVec(statCube(a, b, c), {1, -2}) || compareVec(statCube(a, b, c), {-2, 1}));
}

void check::testStatSquare()
{
    double a = 1, b = 2, c = -3;
    QCOMPARE(-1., statSquare(a, b));

    a = 1;
    b = 0;
    c = -4;
    QCOMPARE(0., statSquare(a, b));
}
