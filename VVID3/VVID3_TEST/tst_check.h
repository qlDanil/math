#ifndef TST_CHECK_H
#define TST_CHECK_H

#include <QtCore>
#include <QtTest/QtTest>

class check : public QObject
{
    Q_OBJECT

public:
    check();

private slots:
    void testLinearEquation();
    void testSquareEquation();
    void testCubeEquation();
    void testElemArithProgress();
    void testSumArithProgress();
    void testSumGeomProgress();
    void testStatCube();
    void testStatSquare();

};

#endif // TST_CHECK_H
