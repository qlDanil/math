/**
*\file
*\brief
Модуль программы
*\author Данил
*\version 1.0
*\example Функция для вычисления корня линейного уравнения.
*
*Все необходимые функции для математических вычислений.
*/

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <QVector>
#include <QtMath>

/**
*\brief Функция для вычисления корня линейного уравнения.
*\param[in] Коэффициент при x
*\param[in] Свободный член
*/
double linearEquation(double a, double b);

/**
*\brief Функция для вычисления корня квадратного уравнения.
*\param[in] Коэффициент при x^2
*\param[in] Коэффициент при x
*\param[in] Свободный член
*/
QVector<double> squareEquation(double a, double b, double c);

/**
*\brief Функция для вычисления корня кубического уравнения.
*\param[in] Коэффициент при x^3
*\param[in] Коэффициент при x^2
*\param[in] Коэффициент при x
*\param[in] Свободный член
*/
QVector<double> cubeEquation(double a, double b, double c, double d);
/**
*\brief Функция для вычисления n-го элемента арифметической прогрессии.
*/
double elemArithProgress(int n, double a, double d);

/**
*\brief Функция для вычисления суммы по n-ый элемент арифметической прогрессии.
*/
double sumArithProgress(int n, double a, double d);

/**
*\brief Функция для вычисления суммы по n-ый элемент геометрической прогрессии.
*/
double sumGeomProgress(int n, double b, double q);

/**
*\brief Функция для вычисления стационарных точек кубической функции.
*/
QVector<double> statCube(double a, double b, double c);

/**
*\brief Функция для вычисления стационарных точек квадратной функции.
*/
double statSquare(double a, double b);


#endif // FUNCTIONS_H
