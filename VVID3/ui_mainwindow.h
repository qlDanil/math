/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QDoubleSpinBox *lineASpin;
    QLabel *label;
    QDoubleSpinBox *lineBSpin;
    QLabel *label_2;
    QPushButton *linePushButton;
    QHBoxLayout *horizontalLayout_2;
    QDoubleSpinBox *squareASpin;
    QLabel *label_3;
    QDoubleSpinBox *squareBSpin;
    QLabel *label_4;
    QDoubleSpinBox *squareCSpin;
    QLabel *label_5;
    QPushButton *squarePushButton;
    QHBoxLayout *horizontalLayout_3;
    QDoubleSpinBox *cubeASpin;
    QLabel *label_6;
    QDoubleSpinBox *cubeBSpin;
    QLabel *label_7;
    QDoubleSpinBox *cubeCSpin;
    QLabel *label_8;
    QDoubleSpinBox *cubeDSpin;
    QLabel *label_9;
    QPushButton *cubePushButton;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_18;
    QPushButton *statCubeButton;
    QPushButton *statSquarePushButton;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_10;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QLabel *label_11;
    QDoubleSpinBox *arithASpin;
    QLabel *label_12;
    QDoubleSpinBox *arithDSpin;
    QLabel *label_13;
    QSpinBox *arithNSpin;
    QPushButton *arithElemPushButton;
    QPushButton *arithSumPushButton;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_14;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton_4;
    QLabel *label_15;
    QDoubleSpinBox *geomBSpin;
    QLabel *label_16;
    QDoubleSpinBox *geomQSpin;
    QLabel *label_17;
    QSpinBox *geomNSpin;
    QPushButton *geomSumPushButton;
    QTextEdit *textEdit;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QButtonGroup *buttonGroup_2;
    QButtonGroup *buttonGroup;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1130, 466);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        lineASpin = new QDoubleSpinBox(centralWidget);
        lineASpin->setObjectName(QStringLiteral("lineASpin"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lineASpin->sizePolicy().hasHeightForWidth());
        lineASpin->setSizePolicy(sizePolicy);
        lineASpin->setMinimum(-99);

        horizontalLayout->addWidget(lineASpin);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);
        QFont font;
        font.setPointSize(12);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        lineBSpin = new QDoubleSpinBox(centralWidget);
        lineBSpin->setObjectName(QStringLiteral("lineBSpin"));
        sizePolicy.setHeightForWidth(lineBSpin->sizePolicy().hasHeightForWidth());
        lineBSpin->setSizePolicy(sizePolicy);
        lineBSpin->setMinimum(-99);

        horizontalLayout->addWidget(lineBSpin);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        sizePolicy1.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy1);
        label_2->setFont(font);

        horizontalLayout->addWidget(label_2);

        linePushButton = new QPushButton(centralWidget);
        linePushButton->setObjectName(QStringLiteral("linePushButton"));

        horizontalLayout->addWidget(linePushButton);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        squareASpin = new QDoubleSpinBox(centralWidget);
        squareASpin->setObjectName(QStringLiteral("squareASpin"));
        sizePolicy.setHeightForWidth(squareASpin->sizePolicy().hasHeightForWidth());
        squareASpin->setSizePolicy(sizePolicy);
        squareASpin->setMinimum(-99);

        horizontalLayout_2->addWidget(squareASpin);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        sizePolicy1.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy1);
        label_3->setFont(font);

        horizontalLayout_2->addWidget(label_3);

        squareBSpin = new QDoubleSpinBox(centralWidget);
        squareBSpin->setObjectName(QStringLiteral("squareBSpin"));
        sizePolicy.setHeightForWidth(squareBSpin->sizePolicy().hasHeightForWidth());
        squareBSpin->setSizePolicy(sizePolicy);
        squareBSpin->setMinimum(-99);

        horizontalLayout_2->addWidget(squareBSpin);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        sizePolicy1.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy1);
        label_4->setFont(font);

        horizontalLayout_2->addWidget(label_4);

        squareCSpin = new QDoubleSpinBox(centralWidget);
        squareCSpin->setObjectName(QStringLiteral("squareCSpin"));
        sizePolicy.setHeightForWidth(squareCSpin->sizePolicy().hasHeightForWidth());
        squareCSpin->setSizePolicy(sizePolicy);
        squareCSpin->setMinimum(-99);

        horizontalLayout_2->addWidget(squareCSpin);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        sizePolicy1.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy1);
        label_5->setFont(font);

        horizontalLayout_2->addWidget(label_5);

        squarePushButton = new QPushButton(centralWidget);
        squarePushButton->setObjectName(QStringLiteral("squarePushButton"));

        horizontalLayout_2->addWidget(squarePushButton);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        cubeASpin = new QDoubleSpinBox(centralWidget);
        cubeASpin->setObjectName(QStringLiteral("cubeASpin"));
        sizePolicy.setHeightForWidth(cubeASpin->sizePolicy().hasHeightForWidth());
        cubeASpin->setSizePolicy(sizePolicy);
        cubeASpin->setMinimum(-99);

        horizontalLayout_3->addWidget(cubeASpin);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        sizePolicy1.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy1);
        label_6->setFont(font);

        horizontalLayout_3->addWidget(label_6);

        cubeBSpin = new QDoubleSpinBox(centralWidget);
        cubeBSpin->setObjectName(QStringLiteral("cubeBSpin"));
        sizePolicy.setHeightForWidth(cubeBSpin->sizePolicy().hasHeightForWidth());
        cubeBSpin->setSizePolicy(sizePolicy);
        cubeBSpin->setMinimum(-99);

        horizontalLayout_3->addWidget(cubeBSpin);

        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        sizePolicy1.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy1);
        label_7->setFont(font);

        horizontalLayout_3->addWidget(label_7);

        cubeCSpin = new QDoubleSpinBox(centralWidget);
        cubeCSpin->setObjectName(QStringLiteral("cubeCSpin"));
        sizePolicy.setHeightForWidth(cubeCSpin->sizePolicy().hasHeightForWidth());
        cubeCSpin->setSizePolicy(sizePolicy);
        cubeCSpin->setMinimum(-99);

        horizontalLayout_3->addWidget(cubeCSpin);

        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        sizePolicy1.setHeightForWidth(label_8->sizePolicy().hasHeightForWidth());
        label_8->setSizePolicy(sizePolicy1);
        label_8->setFont(font);

        horizontalLayout_3->addWidget(label_8);

        cubeDSpin = new QDoubleSpinBox(centralWidget);
        cubeDSpin->setObjectName(QStringLiteral("cubeDSpin"));
        sizePolicy.setHeightForWidth(cubeDSpin->sizePolicy().hasHeightForWidth());
        cubeDSpin->setSizePolicy(sizePolicy);
        cubeDSpin->setMinimum(-99);

        horizontalLayout_3->addWidget(cubeDSpin);

        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        sizePolicy1.setHeightForWidth(label_9->sizePolicy().hasHeightForWidth());
        label_9->setSizePolicy(sizePolicy1);
        label_9->setFont(font);

        horizontalLayout_3->addWidget(label_9);

        cubePushButton = new QPushButton(centralWidget);
        cubePushButton->setObjectName(QStringLiteral("cubePushButton"));

        horizontalLayout_3->addWidget(cubePushButton);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        label_18 = new QLabel(centralWidget);
        label_18->setObjectName(QStringLiteral("label_18"));
        sizePolicy1.setHeightForWidth(label_18->sizePolicy().hasHeightForWidth());
        label_18->setSizePolicy(sizePolicy1);

        horizontalLayout_6->addWidget(label_18);

        statCubeButton = new QPushButton(centralWidget);
        statCubeButton->setObjectName(QStringLiteral("statCubeButton"));

        horizontalLayout_6->addWidget(statCubeButton);

        statSquarePushButton = new QPushButton(centralWidget);
        statSquarePushButton->setObjectName(QStringLiteral("statSquarePushButton"));

        horizontalLayout_6->addWidget(statSquarePushButton);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        sizePolicy1.setHeightForWidth(label_10->sizePolicy().hasHeightForWidth());
        label_10->setSizePolicy(sizePolicy1);

        horizontalLayout_4->addWidget(label_10);

        radioButton = new QRadioButton(centralWidget);
        buttonGroup = new QButtonGroup(MainWindow);
        buttonGroup->setObjectName(QStringLiteral("buttonGroup"));
        buttonGroup->addButton(radioButton);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        sizePolicy.setHeightForWidth(radioButton->sizePolicy().hasHeightForWidth());
        radioButton->setSizePolicy(sizePolicy);

        horizontalLayout_4->addWidget(radioButton);

        radioButton_2 = new QRadioButton(centralWidget);
        buttonGroup->addButton(radioButton_2);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));
        sizePolicy.setHeightForWidth(radioButton_2->sizePolicy().hasHeightForWidth());
        radioButton_2->setSizePolicy(sizePolicy);
        radioButton_2->setChecked(true);

        horizontalLayout_4->addWidget(radioButton_2);

        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        sizePolicy1.setHeightForWidth(label_11->sizePolicy().hasHeightForWidth());
        label_11->setSizePolicy(sizePolicy1);

        horizontalLayout_4->addWidget(label_11);

        arithASpin = new QDoubleSpinBox(centralWidget);
        arithASpin->setObjectName(QStringLiteral("arithASpin"));
        sizePolicy.setHeightForWidth(arithASpin->sizePolicy().hasHeightForWidth());
        arithASpin->setSizePolicy(sizePolicy);
        arithASpin->setMinimum(-99);

        horizontalLayout_4->addWidget(arithASpin);

        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        sizePolicy1.setHeightForWidth(label_12->sizePolicy().hasHeightForWidth());
        label_12->setSizePolicy(sizePolicy1);

        horizontalLayout_4->addWidget(label_12);

        arithDSpin = new QDoubleSpinBox(centralWidget);
        arithDSpin->setObjectName(QStringLiteral("arithDSpin"));
        sizePolicy.setHeightForWidth(arithDSpin->sizePolicy().hasHeightForWidth());
        arithDSpin->setSizePolicy(sizePolicy);
        arithDSpin->setMinimum(-99);

        horizontalLayout_4->addWidget(arithDSpin);

        label_13 = new QLabel(centralWidget);
        label_13->setObjectName(QStringLiteral("label_13"));
        sizePolicy1.setHeightForWidth(label_13->sizePolicy().hasHeightForWidth());
        label_13->setSizePolicy(sizePolicy1);

        horizontalLayout_4->addWidget(label_13);

        arithNSpin = new QSpinBox(centralWidget);
        arithNSpin->setObjectName(QStringLiteral("arithNSpin"));
        sizePolicy.setHeightForWidth(arithNSpin->sizePolicy().hasHeightForWidth());
        arithNSpin->setSizePolicy(sizePolicy);
        arithNSpin->setMinimum(1);

        horizontalLayout_4->addWidget(arithNSpin);

        arithElemPushButton = new QPushButton(centralWidget);
        arithElemPushButton->setObjectName(QStringLiteral("arithElemPushButton"));

        horizontalLayout_4->addWidget(arithElemPushButton);

        arithSumPushButton = new QPushButton(centralWidget);
        arithSumPushButton->setObjectName(QStringLiteral("arithSumPushButton"));

        horizontalLayout_4->addWidget(arithSumPushButton);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_14 = new QLabel(centralWidget);
        label_14->setObjectName(QStringLiteral("label_14"));
        sizePolicy1.setHeightForWidth(label_14->sizePolicy().hasHeightForWidth());
        label_14->setSizePolicy(sizePolicy1);

        horizontalLayout_5->addWidget(label_14);

        radioButton_3 = new QRadioButton(centralWidget);
        buttonGroup_2 = new QButtonGroup(MainWindow);
        buttonGroup_2->setObjectName(QStringLiteral("buttonGroup_2"));
        buttonGroup_2->addButton(radioButton_3);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));
        sizePolicy.setHeightForWidth(radioButton_3->sizePolicy().hasHeightForWidth());
        radioButton_3->setSizePolicy(sizePolicy);

        horizontalLayout_5->addWidget(radioButton_3);

        radioButton_4 = new QRadioButton(centralWidget);
        buttonGroup_2->addButton(radioButton_4);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));
        sizePolicy.setHeightForWidth(radioButton_4->sizePolicy().hasHeightForWidth());
        radioButton_4->setSizePolicy(sizePolicy);
        radioButton_4->setChecked(true);

        horizontalLayout_5->addWidget(radioButton_4);

        label_15 = new QLabel(centralWidget);
        label_15->setObjectName(QStringLiteral("label_15"));
        sizePolicy1.setHeightForWidth(label_15->sizePolicy().hasHeightForWidth());
        label_15->setSizePolicy(sizePolicy1);

        horizontalLayout_5->addWidget(label_15);

        geomBSpin = new QDoubleSpinBox(centralWidget);
        geomBSpin->setObjectName(QStringLiteral("geomBSpin"));
        sizePolicy.setHeightForWidth(geomBSpin->sizePolicy().hasHeightForWidth());
        geomBSpin->setSizePolicy(sizePolicy);
        geomBSpin->setMinimum(-99);

        horizontalLayout_5->addWidget(geomBSpin);

        label_16 = new QLabel(centralWidget);
        label_16->setObjectName(QStringLiteral("label_16"));
        sizePolicy1.setHeightForWidth(label_16->sizePolicy().hasHeightForWidth());
        label_16->setSizePolicy(sizePolicy1);

        horizontalLayout_5->addWidget(label_16);

        geomQSpin = new QDoubleSpinBox(centralWidget);
        geomQSpin->setObjectName(QStringLiteral("geomQSpin"));
        sizePolicy.setHeightForWidth(geomQSpin->sizePolicy().hasHeightForWidth());
        geomQSpin->setSizePolicy(sizePolicy);
        geomQSpin->setMinimum(-99);

        horizontalLayout_5->addWidget(geomQSpin);

        label_17 = new QLabel(centralWidget);
        label_17->setObjectName(QStringLiteral("label_17"));
        sizePolicy1.setHeightForWidth(label_17->sizePolicy().hasHeightForWidth());
        label_17->setSizePolicy(sizePolicy1);

        horizontalLayout_5->addWidget(label_17);

        geomNSpin = new QSpinBox(centralWidget);
        geomNSpin->setObjectName(QStringLiteral("geomNSpin"));
        sizePolicy.setHeightForWidth(geomNSpin->sizePolicy().hasHeightForWidth());
        geomNSpin->setSizePolicy(sizePolicy);
        geomNSpin->setMinimum(1);

        horizontalLayout_5->addWidget(geomNSpin);

        geomSumPushButton = new QPushButton(centralWidget);
        geomSumPushButton->setObjectName(QStringLiteral("geomSumPushButton"));

        horizontalLayout_5->addWidget(geomSumPushButton);


        verticalLayout->addLayout(horizontalLayout_5);

        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));

        verticalLayout->addWidget(textEdit);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1130, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        label->setText(QApplication::translate("MainWindow", "x + ", nullptr));
        label_2->setText(QApplication::translate("MainWindow", " = 0;", nullptr));
        linePushButton->setText(QApplication::translate("MainWindow", "\321\200\320\265\321\210\320\270\321\202\321\214 \320\273\320\270\320\275\320\265\320\271\320\275\320\276\320\265 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\320\265", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "x^2 + ", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "x + ", nullptr));
        label_5->setText(QApplication::translate("MainWindow", " = 0;", nullptr));
        squarePushButton->setText(QApplication::translate("MainWindow", "\321\200\320\265\321\210\320\270\321\202\321\214 \320\272\320\262\320\260\320\264\321\200\320\260\321\202\320\275\320\276\320\265 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\320\265", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "x^3 + ", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "x^2 + ", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "x + ", nullptr));
        label_9->setText(QApplication::translate("MainWindow", " = 0;", nullptr));
        cubePushButton->setText(QApplication::translate("MainWindow", "\321\200\320\265\321\210\320\270\321\202\321\214 \320\272\321\203\320\261\320\270\321\207\320\265\321\201\320\272\320\276\320\265 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\320\265", nullptr));
        label_18->setText(QApplication::translate("MainWindow", "\320\235\320\260\320\271\321\202\320\270 \321\201\321\202\320\260\321\206\320\270\320\276\320\275\320\260\321\200\320\275\321\213\320\265 \321\202\320\276\321\207\320\272\320\270, \320\262\320\267\321\217\320\262 \320\267\320\260 \321\204\321\203\320\275\320\272\321\206\320\270\321\216 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\320\265, \320\262\320\262\320\265\320\264\320\265\320\275\320\275\320\276\320\265 \320\262\321\213\321\210\320\265", nullptr));
        statCubeButton->setText(QApplication::translate("MainWindow", "\320\272\321\203\320\261\320\270\321\207\320\265\321\201\320\272\320\276\320\265", nullptr));
        statSquarePushButton->setText(QApplication::translate("MainWindow", "\320\272\320\262\320\260\320\264\321\200\320\260\321\202\320\275\320\276\320\265", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "\320\220\321\200\320\270\321\204\320\274\320\265\321\202\320\270\321\207\320\265\321\201\320\272\321\203\321\216 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\321\216 \320\267\320\260\320\264\320\260\321\202\321\214", nullptr));
        radioButton->setText(QApplication::translate("MainWindow", "\321\201\320\273\321\203\321\207\320\260\320\271\320\275\320\276", nullptr));
        radioButton_2->setText(QApplication::translate("MainWindow", "\320\262\321\200\321\203\321\207\320\275\321\203\321\216", nullptr));
        label_11->setText(QApplication::translate("MainWindow", " \320\275\320\260\321\207\320\260\320\273\321\214\320\275\321\213\320\271 \321\215\320\273\320\265\320\274\320\265\320\275\321\202 = ", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "\321\200\320\260\320\267\320\275\320\276\321\201\321\202\321\214 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\320\270 = ", nullptr));
        label_13->setText(QApplication::translate("MainWindow", " \320\275\320\276\320\274\320\265\321\200 \320\266\320\265\320\273\320\260\320\265\320\274\320\276\320\263\320\276 \321\215\320\273\320\265\320\274\320\265\320\275\321\202\320\260 = ", nullptr));
        arithElemPushButton->setText(QApplication::translate("MainWindow", "\320\262\321\213\320\262\320\265\321\201\321\202\320\270 \321\215\320\273\320\265\320\274\320\265\320\275\321\202", nullptr));
        arithSumPushButton->setText(QApplication::translate("MainWindow", "\320\262\321\213\320\262\320\265\321\201\321\202\320\270 \321\201\321\203\320\274\320\274\321\203", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "\320\223\320\265\320\276\320\274\320\265\321\202\321\200\320\270\321\207\320\265\321\201\320\272\321\203\321\216 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\321\216 \320\267\320\260\320\264\320\260\321\202\321\214", nullptr));
        radioButton_3->setText(QApplication::translate("MainWindow", "\321\201\320\273\321\203\321\207\320\260\320\271\320\275\320\276", nullptr));
        radioButton_4->setText(QApplication::translate("MainWindow", "\320\262\321\200\321\203\321\207\320\275\321\203\321\216", nullptr));
        label_15->setText(QApplication::translate("MainWindow", " \320\275\320\260\321\207\320\260\320\273\321\214\320\275\321\213\320\271 \321\215\320\273\320\265\320\274\320\265\320\275\321\202 =  ", nullptr));
        label_16->setText(QApplication::translate("MainWindow", " \320\267\320\275\320\260\320\274\320\265\320\275\320\260\321\202\320\265\320\273\321\214 = ", nullptr));
        label_17->setText(QApplication::translate("MainWindow", "\320\275\320\276\320\274\320\265\321\200 \320\266\320\265\320\273\320\260\320\265\320\274\320\276\320\263\320\276 \321\215\320\273\320\265\320\274\320\265\320\275\321\202\320\260 = ", nullptr));
        geomSumPushButton->setText(QApplication::translate("MainWindow", "\320\262\321\213\320\262\320\265\321\201\321\202\320\270 \321\201\321\203\320\274\320\274\321\203", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
