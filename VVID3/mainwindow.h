#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "functions.h"
#include <QMainWindow>
#include <QDateTime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_linePushButton_clicked();

    void on_squarePushButton_clicked();

    void on_cubePushButton_clicked();

    void on_arithElemPushButton_clicked();

    void on_arithSumPushButton_clicked();

    void on_geomSumPushButton_clicked();

    void on_statSquarePushButton_clicked();

    void on_statCubeButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
